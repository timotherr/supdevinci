# Projet : Who Run The World

**Auteurs :** Marlon GAMARUS / Timothé RADIGOY  
**Date :** 16/11/23

## Table des matières
1. [Introduction](#1-introduction)
2. [Création et Push d'Images](#2-création-et-push-dimages)
3. [Vérification sur la Registry](#3-vérification-sur-la-registry)
4. [Déploiement avec Kubernetes](#4-déploiement-avec-kubernetes)
5. [ConfigMap et Secrets dans Kubernetes](#5-configmap-et-secrets-dans-kubernetes)
6. [CI/CD Pipeline](#6-cicd-pipeline)
7. [Espaces de Noms Kubernetes](#7-espaces-de-noms-kubernetes)
8. [Déploiement avec Terraform](#8-déploiement-avec-terraform)

---

## 1. Introduction
Présentation du projet "Who Run The World", objectifs, contexte, et importance.
Les principaux objectifs de ce projet sont :
Développer une Compréhension Pratique : Fournir une expérience concrète dans l'utilisation de technologies telles que Docker, Kubernetes, et les pipelines CI/CD.
Illustrer l'Architecture des Applications Distribuées : Démontrer comment différentes technologies et composants logiciels peuvent être intégrés pour créer une application robuste et évolutive.
Mise en Pratique des Bonnes Pratiques : Mettre en œuvre des pratiques de développement, de déploiement, et de sécurité reconnue dans l'industrie.

![Alt text](image.png)

Structure du Projet 

![Alt text](image-1.png)
## 2. Création et Push d'Images
- Authentification: `docker login -u oauth2 -p glpat-_vMxrgJK9tk_NyNvLGax registry.gitlab.com`

![Alt text](image-2.png)

- Construction: `docker build -t nom_de_l_image .`
- Tag: `docker tag healthchecks registry.gitlab.com/timotherr/supdevinci/healthchecks`
- Push: `docker push registry.gitlab.com/timotherr/supdevinci`

## 3. Vérification sur la Registry
Connectez-vous à la registry Docker pour vérifier le push des images.

## 4. Déploiement avec Kubernetes
- ClusterIP et NodePort services.
- Manifeste: `kubectl apply -f service-clusterip.yaml` et `kubectl apply -f service-nodeport.yaml`

![Alt text](image-3.png)

![Alt text](image-4.png)
- ConfigMaps et Secrets: `kubectl create secret generic pgpassword --from-literal PGPASSWORD=mypassword`

## 5. ConfigMap et Secrets dans Kubernetes
Gestion des configurations et protection des données sensibles.

## 6. CI/CD Pipeline
Description du pipeline CI/CD

![Alt text](image-5.png)

automatisation des tests, construction, et déploiement.

![Alt text](image-6.png)

Explications :
-	Stages : Définit les étapes de la pipeline (déploiement dans l'environnement de développement et de production).

-	Variables : Définit des variables, ici, le nom de l'image Docker et le driver Docker.

-	Before_script : Les commandes à exécuter avant chaque script de chaque étape de la pipeline, ici, la connexion au registre Docker GitLab.

-	Deploy_dev : Étape de déploiement dans l'environnement de développement. Elle définit le contexte Kubernetes pour l'environnement de développement (dev) et applique le manifeste de déploiement (deployment-dev.yaml).

-	Deploy_prod : Étape de déploiement dans l'environnement de production. Elle définit le contexte Kubernetes pour l'environnement de production (prod) et applique le manifeste de déploiement (deployment-prod.yaml).
Exécution des Tests Automatisés :
Les tests unitaires et d'intégration sont exécutés pour s'assurer que le nouveau code ne casse pas l'application.
Stockage des Images Docker :
Les images construites sont poussées vers un registre d'images, tel que Docker Hub qui est géré par la pipeline.


![Alt text](image-7.png)

## 7. Espaces de Noms Kubernetes
Créez un fichier YAML nommé, par exemple, namespace-dev.yaml, avec les informations suivantes :

 ![Alt text](image-8.png)

 ![Alt text](image-9.png)

Appliquez le manifeste en utilisant la commande suivante :
kubectl apply -f namespace-dev.yaml
kubectl apply -f namespace-prod.yaml


Pour mettre en place les newspace :
kubectl config set-context --current --namespace=dev
kubectl config set-context --current --namespace=prod

Déploiement dans l'Environnement de Test :
Déployer l'application dans un environnement de test Kubernetes.
Valider le déploiement et surveiller son comportement.
Approbation pour le Déploiement en Production :
Après validation dans l'environnement de test, un processus d'approbation est requis pour le déploiement en production.
Stratégies de Déploiement :
Implémentation de stratégies de déploiement telles que le déploiement progressif (RollingUpdate) pour minimiser les interruptions.
Mise en place de tests de santé et de sondes de disponibilité pour assurer le bon fonctionnement de l'application après le déploiement.


## 8. Déploiement avec Terraform
- Terraform init et apply.
- Intégration avec CI/CD.
Créez un fichier Terraform nommé, main.tf, qui contiendra la configuration de vos machines virtuelles. 

![Alt text](image-10.png)

Le fichier var :

![Alt text](image-11.png)

Dans le répertoire où se trouvent vos fichiers Terraform, exécutez la commande suivante pour initialiser Terraform :
terraform init
Avant de déployer, assurez-vous que la configuration est correcte en exécutant :
terraform validate
Déployez vos machines virtuelles en exécutant :
terraform apply
